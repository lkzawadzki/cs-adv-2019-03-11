﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTreeVisitor
{
    class Program
    {
        public static int Square(int a)
        {
            return a * a;
        }

        static void Main(string[] args)
        {
            Expression<Func<int, int>> expression = n => 1 + Square(n * ((n % 2 == 0) ? -1 : 1));

            DisplayVisitor visitor = new DisplayVisitor();

            visitor.Visit(expression);

            Console.ReadLine();
        }

        class DisplayVisitor : ExpressionVisitor
        {
            private int level = 0;

            public override Expression Visit(Expression node)
            {
                if (node != null)
                {
                    for (int i = 0; i < level; i++)
                    {
                        Console.Write("   ");
                    }

                    Console.WriteLine($"{node.NodeType} - {node.GetType().Name}");
                }

                level++;
                Expression result = base.Visit(node);
                level--;

                return result;
            }
        }
    }
}
