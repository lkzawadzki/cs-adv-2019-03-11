﻿using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDemo
{
    public class EmployeeCollection : IEnumerable
    {
        private ArrayList employee = new ArrayList();

        public void AddEmployee(Employee emp)
        {
            employee.Add(emp);
        }

        public Employee GetEmployee(int position)
        {
            return (Employee)employee[position];
        }

        public IEnumerator GetEnumerator()
        {
            //return employee.GetEnumerator();

            foreach (var item in employee)
            {
                yield return item;
            }
        }
    }
}
