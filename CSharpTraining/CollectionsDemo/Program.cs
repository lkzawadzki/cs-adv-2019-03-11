﻿using Models;
using System;
using System.Collections.Generic;

namespace CollectionsDemo
{
    class Program
    {
        static Employee[] employees = {
            new Employee(1, "Robert", PerformanceGrade.Good, 5000M),
            new Employee(2, "Anna", PerformanceGrade.Excellent, 6000M),
            new Employee(3, "Ewa", PerformanceGrade.Decent, 4000M)};

        static void Main(string[] args)
        {
            //NonGenericArray();

            //UseGenericList();
            //Stack();
            //Queue();
            //SortedSet();
            //Dictionary();
            LinkedList();

            Console.ReadLine();
        }

        static void LinkedList()
        {
            LinkedList<Employee> linked = new LinkedList<Employee>();

            linked.AddFirst(employees[2]);
            linked.AddLast(employees[0]);

            linked.AddAfter(linked.First, employees[1]);

            Console.WriteLine(linked.Last.Previous.Value.ToString());

            var node = linked.Find(employees[2]);

            Console.WriteLine(node.Value.ToString());

            linked.Remove(node);
        }

        static void Dictionary()
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            dictionary.Add("One", 1);

            dictionary["One"] = 2;
            dictionary["Two"] = 3;

            Console.WriteLine(dictionary.ContainsKey("Three"));

            Console.WriteLine(dictionary.ContainsValue(3));

            foreach (var item in dictionary)
            {
                Console.WriteLine(item.Key + ", " + item.Value);
            }

            Console.WriteLine();

            foreach (var item in dictionary.Values)
            {
                Console.WriteLine(item);
            }
        }

        static void SortedSet()
        {
            SortedSet<Employee> employeesSorted 
                = new SortedSet<Employee>(new SortByNameLength());

            employeesSorted.Add(employees[0]);
            employeesSorted.Add(employees[2]);
            employeesSorted.Add(employees[1]);

            foreach (var item in employeesSorted)
            {
                Console.WriteLine(item.ToString());
            }

            employeesSorted.Add(employees[1]);

            Console.WriteLine();

            foreach (var item in employeesSorted)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void Queue()
        {
            Queue<Employee> employeesQueue = new Queue<Employee>();

            employeesQueue.Enqueue(employees[0]);
            employeesQueue.Enqueue(employees[2]);
            employeesQueue.Enqueue(employees[1]);

            Console.WriteLine(employeesQueue.Peek().ToString());

            Console.WriteLine();

            while (employeesQueue.Count > 0)
            {
                Console.WriteLine(employeesQueue.Dequeue().ToString());
            }
        }

        static void Stack()
        {
            Stack<Employee> employeesStack = new Stack<Employee>();

            employeesStack.Push(employees[0]);
            employeesStack.Push(employees[2]);
            employeesStack.Push(employees[1]);

            Console.WriteLine(employeesStack.Peek().ToString());

            Console.WriteLine();

            while (employeesStack.Count > 0)
            {
                Console.WriteLine(employeesStack.Pop().ToString());
            }
        }

        static void UseGenericList()
        {
            List<Employee> employeesList = new List<Employee>();

            employeesList.AddRange(employees);

            foreach (var item in employeesList)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();

            Console.WriteLine(employeesList[2]);

            employeesList.Add(new Employee(3, "Monika", PerformanceGrade.Good, 5000M));

            Console.WriteLine();

            foreach (var item in employeesList)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void BoxingDemo()
        {
            int i = 10;

            object b = i;

            int i1 = (int)b;
        }

        static void NonGenericArray()
        {
            //ArrayList arrayList = new ArrayList();
            EmployeeCollection arrayList = new EmployeeCollection();

            //arrayList.AddRange(new Employee[]{
            //    new Employee(1, "Robert"),
            //    new Employee(2, "Annna")
            //});

            arrayList.AddEmployee(new Employee(1, "Robert"));
            arrayList.AddEmployee(new Employee(2, "Anna"));

            //Console.WriteLine(arrayList.Count);

            //arrayList.Add(5);

            foreach (var item in arrayList)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();

            //arrayList.Reverse();

            foreach (var item in arrayList)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
