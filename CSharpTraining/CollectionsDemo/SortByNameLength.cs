﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDemo
{
    public class SortByNameLength : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            if (x.Name.Length > y.Name.Length)
            {
                return 1;
            }
            else if(x.Name.Length < y.Name.Length)
            {
                return -1;
            }
            else
            {
                return string.Compare(x.Name, y.Name);
            }
        }
    }
}
