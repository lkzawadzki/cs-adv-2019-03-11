﻿using System;

namespace LinqToEF
{
    public class Product
    {
        public Product()
        {

        }

        public int ProductID { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime DateCreated { get; set; }

        public int StoreID { get; set; }
        public Store Store { get; set; }
    }
}