﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToEF
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<StoreDbContext>());

            //InsertData();
            QueryData();

            //Console.WriteLine("Prawdopodobnie się powiodło");
            Console.ReadLine();
        }

        static void QueryData()
        {
            using(StoreDbContext context = new StoreDbContext())
            {
                var query = context.Products.Where(p => p.DateCreated.Year == 2019);

                context.Database.Log = Console.WriteLine;

                foreach (var item in query)
                {
                    Console.WriteLine(item.Description + " " + item.DateCreated);
                }
            }
        }

        static void InsertData()
        {
            using(StoreDbContext context = new StoreDbContext())
            {
                Store s1 = new Store { Name = "Amazon" };
                Store s2 = new Store { Name = "Ebay" };

                Product p1 = new Product {
                    Description = "IPhone",
                    Price = 2500M,
                    DateCreated = new DateTime(2018, 2, 3) };
                Product p2 = new Product
                {
                    Description = "Huawei",
                    Price = 500M,
                    DateCreated = new DateTime(2018, 10, 4)
                };
                Product p3 = new Product
                {
                    Description = "HTC",
                    Price = 1500M,
                    DateCreated = new DateTime(2017, 6, 8)
                };
                Product p4 = new Product
                {
                    Description = "Sony",
                    Price = 1400M,
                    DateCreated = new DateTime(2019, 1, 31)
                };
                Product p5 = new Product
                {
                    Description = "Samsung",
                    Price = 1800M,
                    DateCreated = new DateTime(2018, 12, 22)
                };
                Product p6 = new Product
                {
                    Description = "HTC",
                    Price = 1400M,
                    DateCreated = new DateTime(2018, 10, 4)
                };
                Product p7 = new Product
                {
                    Description = "Huwaei",
                    Price = 500M,
                    DateCreated = new DateTime(2018, 10, 4)
                };

                context.Stores.Add(s1);
                context.Stores.Add(s2);

                s1.Products.AddRange(new List<Product> { p1, p3, p5, p7 });
                s2.Products.AddRange(new List<Product> { p2, p4, p6 });

                context.SaveChanges();
            }
        }
    }
}
