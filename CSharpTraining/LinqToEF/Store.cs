﻿using System.Collections.Generic;

namespace LinqToEF
{
    public class Store
    {
        public Store()
        {
            Products = new List<Product>();
        }

        public int ID { get; set; }
        public string Name { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
