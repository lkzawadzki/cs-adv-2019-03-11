﻿using System.Data.Entity;

namespace LinqToEF
{
    public class StoreDbContext : DbContext
    {
        public DbSet<Store> Stores { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
