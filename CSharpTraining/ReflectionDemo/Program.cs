﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee(1, "Marta");

            Type t1 = emp1.GetType();

            Console.WriteLine(t1.Name);

            Type t2 = typeof(Employee);

            Console.WriteLine(t2.Name);

            Type t3 = Type.GetType("Models.Employee, Models");

            Console.WriteLine(t3.Name);

            Console.ReadLine();
        }
    }
}
