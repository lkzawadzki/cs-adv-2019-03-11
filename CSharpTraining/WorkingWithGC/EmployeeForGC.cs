﻿using Microsoft.Win32.SafeHandles;
using Models;
using System;
using System.Runtime.InteropServices;

namespace WorkingWithGC
{
    public class EmployeeForGC : Employee, IDisposable
    {
        //niezarządzany zasób
        public IntPtr Info;
        //zarządzany zasób
        public SafeHandle Handle;

        public EmployeeForGC(int id, string name, PerformanceGrade grade, decimal salary) 
            : base(id, name, grade, salary)
        {
            Info = Marshal.StringToHGlobalAnsi($"{id};{name};{grade};{salary}");
            Handle = new SafeFileHandle(new IntPtr(), true);
        }

        private bool disposed;

        ~EmployeeForGC()
        {
            Dispose(false);
        }

        protected void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //zwolnienie zarządzane zasoby
                Handle.Dispose();
            }

            //zwolnienie niezarządzanych zasobów
            Marshal.FreeHGlobal(Info);

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
