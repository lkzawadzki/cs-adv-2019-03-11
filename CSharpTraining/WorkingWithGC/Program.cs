﻿using Models;
using System;
using System.Runtime.InteropServices;

namespace WorkingWithGC
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateEmployye();

            //poza zakresem CreateEmployee
            EmployeeForGC emp = new EmployeeForGC(2, "Robert", PerformanceGrade.Bad, 5000M);

            Console.WriteLine($"Obiekt {emp} znajduje się w generacji {GC.GetGeneration(emp)}");
            
            Console.WriteLine(emp.ToString());
            Console.WriteLine(Marshal.PtrToStringAnsi(emp.Info));

            //Console.WriteLine("Wywołano GC!");

            using (EmployeeForGC emp3 = 
                new EmployeeForGC(1, "Janusz", PerformanceGrade.Excellent, 10000M))
            {
                Console.WriteLine(emp3.ToString());
            }

            EmployeeForGC emp4 = new EmployeeForGC(1, "Janusz", PerformanceGrade.Excellent, 10000M);
            try
            {
                Console.WriteLine(emp4.ToString());
            }
            finally
            {
                emp4.Dispose();
            }

            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            Console.WriteLine(Marshal.PtrToStringAnsi(emp.Info));

            Console.WriteLine($"Obiekt {emp} znajduje się w generacji {GC.GetGeneration(emp)}");

            Console.ReadLine();
        }

        static void CreateEmployye()
        {
            Employee e1 = new Employee(1, "Marta", PerformanceGrade.Good, 3000M);

            Console.WriteLine(e1.ToString());
        }
    }
}
