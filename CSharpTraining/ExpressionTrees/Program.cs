﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTrees
{
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, int, int> multiply = (x, y) => x * y;

            Expression<Func<int, int, int>> multiply2 = (x, y) => x * y;

            int result1 = multiply(10, 20);

            Console.WriteLine(result1);

            int result2 = multiply2.Compile()(10, 20);

            Console.WriteLine(result2);

            Console.WriteLine();

            Console.WriteLine(multiply2.NodeType);

            Console.WriteLine();

            Console.WriteLine(multiply2.Body.NodeType);

            Console.WriteLine();

            Console.WriteLine(multiply2.Body);

            BinaryExpression binary = (BinaryExpression)multiply2.Body;

            Console.WriteLine(binary.Left);
            Console.WriteLine(binary.Right);

            Console.ReadLine();
        }
    }
}
