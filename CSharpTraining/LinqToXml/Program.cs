﻿using LinqToEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreatingXml();

            //Navigation();

            ModifyXml();

            Console.ReadLine();
        }

        static void ModifyXml()
        {
            XDocument doc = XDocument.Load("employees.xml");

            var query1 = doc.Root.Element("director");

            XElement elem1 = new XElement("senior-programmer",
                new XElement("name", "Piotr Zachara"), new XElement("id", 2));

            var query = doc.Descendants("senior-programmer");

            //doc.Root.Add(elem1);

            query1.ReplaceWith(elem1);

            foreach (var item in query)
            {
                var elements = item.Elements();

                item.SetElementValue("id", 2);

                item.SetAttributeValue("stan", "inactive");

                foreach (var item1 in elements)
                {
                }
            }

            doc.Save("employees_modified.xml");
        }

        static void Navigation()
        {
            XDocument doc = XDocument.Load("employees.xml");

            foreach (XElement item in doc.Root.Elements())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var node = doc.Root.FirstNode;

            Console.WriteLine(node.ToString());

            Console.WriteLine();

            var elements = doc.Root.Elements("programmer");

            foreach (var item in elements)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();

            var element = doc.Root.Element("programmer");

            Console.WriteLine(element);

            Console.WriteLine();

            var query = from e in doc.Root.Elements()
                        where e.Attribute("state").Value == "active"
                        select e;

            var fluent = doc.Root.Elements().Where(e => e.Attribute("state").Value == "inactive");

            foreach (var item in fluent)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query1 = doc.Root.DescendantNodes().OfType<XComment>();

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var element1 = element.Element("name");

            Console.WriteLine(element1.Parent);

            Console.WriteLine();

            foreach (var item in element1.Ancestors())
            {
                Console.WriteLine(item.Name);
            }
        }

        static void CreatingXml()
        {
            string xml = @"<employee id='1'>
                            <firstName>Marcin</firstName>
                            <lastName>Nowak</lastName>
                            </employee>";

            XDocument xdoc = XDocument.Parse(xml);

            foreach (XElement item in xdoc.Elements())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            Console.WriteLine(xdoc.ToString(SaveOptions.DisableFormatting));

            XDocument xdoc1 = XDocument.Load("http://infotraining.pl/sitemap.xml");

            foreach (XElement item in xdoc1.Elements())
            {
                Console.WriteLine(item);
            }

            XElement employees = new XElement("employees", new XComment("lista pracowników"),
                new XElement("manager", new XAttribute("state", "active"),
                    new XElement("name", "Marcin Nowak")),
                new XElement("director", new XAttribute("state", "active"),
                    new XElement("name", "Ewa Białkowska")),
                new XElement("programmer", new XAttribute("state", "active"),
                    new XElement("name", "Krzysztof Kowalski")),
                new XElement("programmer", new XAttribute("state", "inactive"),
                    new XElement("name", "Maciej Czepiel"))
                );

            XDocument xdoc2 = new XDocument(employees);

            xdoc2.Save("employees.xml");

            using (StoreDbContext context = new StoreDbContext())
            {
                var query = new XElement("stores",
                                         from s in context.Stores.ToList()
                                         select new XElement(s.Name, new XAttribute("id", s.ID),
                                            from p in s.Products
                                            select new XElement(p.Description,
                                                    new XAttribute("price", p.Price))));

                XDocument doc = new XDocument(query);

                doc.Save("stores.xml");
            }
        }
    }
}
