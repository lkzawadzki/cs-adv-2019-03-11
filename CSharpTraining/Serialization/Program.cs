﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee(1, "Grażyna", PerformanceGrade.Excellent, 7000M);

            Console.WriteLine("Serializacja Binary:");
            IFormatter bf = new BinaryFormatter();
            Serialize(emp1, "employee.dat", bf);

            Employee emp2 = Deserialize("employee.dat", bf);

            Console.WriteLine(emp1.ToString() + $", id kontraktu: {emp1.Contract.GetContractId()}");
            Console.WriteLine(emp2.ToString() + $", id kontraktu: {emp2.Contract.GetContractId()}");

            Console.WriteLine();

            Console.WriteLine("Serializacja Soap");
            IFormatter sf = new SoapFormatter();
            Serialize(emp1, "employee.soap", sf);

            Employee emp3 = Deserialize("employee.soap", sf);

            Console.WriteLine(emp1.ToString() + $", id kontraktu: {emp1.Contract.GetContractId()}");
            Console.WriteLine(emp3.ToString() + $", id kontraktu: {emp3.Contract.GetContractId()}");

            Console.WriteLine();

            Console.WriteLine("Serializacja Xml:");
            Serialize(emp1, "employee.xml");

            Employee emp4 = Deserialize("employee.xml");

            Console.WriteLine(emp1.ToString() + $", id kontraktu: {emp1.Contract.GetContractId()}");
            Console.WriteLine(emp4.ToString() + $", id kontraktu: {emp4.Contract.GetContractId()}");

            Console.WriteLine();

            Console.WriteLine("Serializacja Json:");
            Employee emp5 = JsonSerialize(emp1);

            Console.WriteLine(emp1.ToString() + $", id kontraktu: {emp1.Contract.GetContractId()}");
            Console.WriteLine(emp5.ToString() + $", id kontraktu: {emp5.Contract.GetContractId()}");

            Console.WriteLine();

            Console.WriteLine("Serializacja kolekcji:");

            List<Employee> employees = new List<Employee> { emp1, emp2, emp3, emp4, emp5 };
            
            using(FileStream fs = new FileStream("employees.dat", FileMode.Create, FileAccess.Write))
            {
                bf.Serialize(fs, employees);
            }

            List<Employee> employees1 = new List<Employee>();

            using(FileStream fs = new FileStream("employees.dat", FileMode.Open, FileAccess.Read))
            {
                employees1 = (List<Employee>)bf.Deserialize(fs);
            }

            foreach (var item in employees1)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadLine();
        }

        static Employee JsonSerialize(Employee emp)
        {
            JsonSerializer js = new JsonSerializer();

            using (StreamWriter sw = new StreamWriter("employee.json"))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                js.Serialize(jw, emp);
            }

            Employee e1;

            using (StreamReader sr = new StreamReader("employee.json"))
            using (JsonReader jr = new JsonTextReader(sr))
            {
                e1 = js.Deserialize<Employee>(jr);
            }

            return e1;
        }

        static void Serialize(Employee emp, string filename, IFormatter formatter = null)
        {
            if (formatter != null)
            {
                using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    formatter.Serialize(fs, emp);
                }
            }
            else
            {
                XmlSerializer xs = new XmlSerializer(typeof(Employee));

                using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    xs.Serialize(fs, emp);
                }
            }
        }

        static Employee Deserialize(string filename, IFormatter formatter = null)
        {
            if (formatter != null)
            {
                Employee e;

                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    e = (Employee)formatter.Deserialize(fs);
                }

                return e;
            }

            XmlSerializer xs = new XmlSerializer(typeof(Employee));

            Employee e1;

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                e1 = (Employee)xs.Deserialize(fs);
            }

            return e1;
        }
    }
}
