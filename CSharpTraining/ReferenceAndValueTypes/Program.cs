﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceAndValueTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            //ValueTypes();
            ReferenceTypes();

            //Employee emp = new Employee(2, "Ewa");

            //PassReferenceType(ref emp);

            //Console.WriteLine(emp.Name);

            Console.ReadLine();
        }

        static void PassReferenceType(ref Employee emp)
        {
            emp.Name = "Monika";

            emp = new Employee(3, "Adam");
        }

        static void ValueTypes()
        {
            int i = 0;
            Point p = new Point(10, 20);

            Point p1 = p;

            p.x = 100;
            p1.info = new PointInfo("nowa informacja");

            p.PrintPosition();
            Console.WriteLine(p.info.Description);
            p1.PrintPosition();
            Console.WriteLine(p1.info.Description);

        } //p zostanie usunięte, i później

        static void ReferenceTypes()
        {
            Employee emp1 = new Employee(1, "Robert", PerformanceGrade.Good);

            Employee emp2 = (Employee)emp1.Clone();

            Console.WriteLine(emp1.Name + " " + emp1.Contract.Salary);
            Console.WriteLine(emp2.Name + " " + emp2.Contract.Salary);

            emp2.Name = "Marcin";
            emp2.Contract.Salary = 5000M;

            Console.WriteLine();
            Console.WriteLine(emp1.Name + " " + emp1.Contract.Salary);
            Console.WriteLine(emp2.Name + " " + emp2.Contract.Salary);
        } // emp2 i emp1 zostaną usunięte?
    }
}
