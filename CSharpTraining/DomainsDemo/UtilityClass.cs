﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainsDemo
{
    public class UtilityClass : MarshalByRefObject
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrintMessage()
        {
            Console.WriteLine("Wywołano z: " + AppDomain.CurrentDomain.FriendlyName);
        }
    }
}
