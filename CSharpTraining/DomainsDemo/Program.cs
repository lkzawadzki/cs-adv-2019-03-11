﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DomainsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayDomain(AppDomain.CurrentDomain);

            Console.WriteLine();

            AppDomain secondDomain = AppDomain.CreateDomain("DrugaDomena");

            try
            {
                secondDomain.Load("LinqBasics");
                secondDomain.ExecuteAssemblyByName("LinqBasics");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine();

            DisplayDomain(secondDomain);

            Console.WriteLine();

            UtilityClass uc = (UtilityClass)secondDomain.CreateInstanceAndUnwrap(
                typeof(UtilityClass).Assembly.FullName,
                typeof(UtilityClass).FullName);

            uc.PrintMessage();

            AppDomain.Unload(secondDomain);

            Console.ReadLine();
        }

        static void DisplayDomain(AppDomain domain)
        {
            Console.WriteLine($"Nazwa: {domain.FriendlyName}");
            Console.WriteLine($"Id: {domain.Id}");
            Console.WriteLine($"Lokalizacja: {domain.BaseDirectory}");

            foreach (Assembly item in domain.GetAssemblies())
            {
                Console.WriteLine(
                    $"  Nazwa: {item.GetName().Name}, wersja: {item.GetName().Version}");
            }
        }
    }
}
