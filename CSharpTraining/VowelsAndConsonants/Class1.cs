﻿using PluginBase;
using System;
using System.Linq;

namespace VowelsAndConsonants
{
    [PluginInfo(Name = "Zostaw samogłoski", Description = "Zostawia samogłoski z zaznaczonego tekstu")]
    public class OnlyVowelsPlugin : MarshalByRefObject, ITextTransform
    {
        public string Transform(string text)
        {
            string vowels = "aeiouy";

            string result = new string(text.Where(c => vowels.Contains(char.ToLower(c))).ToArray());

            return result;
        }
    }

    [PluginInfo(Name = "Zostaw spółgłoski", Description = "Zostawia spółgłoski z zaznaczonego tekstu")]
    public class OnlyConsonantsPlugin : MarshalByRefObject, ITextTransform
    {
        public string Transform(string text)
        {
            string vowels = "aeiouy";

            string result = new string(text.Where(c => !vowels.Contains(char.ToLower(c))).ToArray());

            return result;
        }
    }
}
