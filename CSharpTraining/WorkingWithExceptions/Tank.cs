﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithExceptions
{
    public class Tank
    {
        public Engine Engine;
        public const int CriticalSpeed = 150;
        private int CurrentSpeed;
        public string Name;
        private bool overheated;

        public Tank(string name)
        {
            Name = name;
            Engine = new Engine();
        }

        public void TurnEngine(bool on)
        {
            this.Engine.Power(on);
        }

        public void SpeedUp(int delta)
        {
            if (overheated)
            {
                Console.WriteLine("Czołg się przegrzał i nie działa");
                throw new ArgumentOutOfRangeException();
            }
            else if (!Engine.IsEngineOn()){
                Console.WriteLine("Nie włączyłeś silnika");
                throw new InvalidOperationException();
            }
            else
            {
                CurrentSpeed += delta;

                if (CurrentSpeed > CriticalSpeed)
                {
                    overheated = true;
                    Engine.Power(false);

                    //Exception ex = new Exception($"{Name} się przegrzał!");

                    //ex.Data.Add("TimeStamp", DateTime.Now);
                    //ex.Data.Add("Cause", "Została przekroczona prędkość maksymalna " + CriticalSpeed);

                    //throw ex;

                    throw new TankOverheatedException($"{Name} się przegrzał!",
                        "Została przekroczona prędkość maksymalna " + CriticalSpeed, DateTime.Now);
                }
                else
                {
                    Console.WriteLine($"> prędkość: {CurrentSpeed}");
                }
            }
        }

    }
}
