﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Tank t1 = new Tank("Leopard");
            t1.Engine.Power(true);

            try
            {
                for (int i = 0; i < 20; i++)
                {
                    t1.SpeedUp(10);
                }
            }
            catch (TankOverheatedException ex) when (ex.TimeStamp.DayOfWeek != DayOfWeek.Monday)
            {
                try
                {
                    FileStream fs = File.Open(@"c:\tank_log.xml", FileMode.Open);
                }
                catch (FileNotFoundException)
                {
                    throw new FileNotFoundException("Plik nie został znaleziony!", ex);
                }

                Console.WriteLine("\nBłąd!!!");

                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
                Console.WriteLine(ex.TargetSite.DeclaringType);
                Console.WriteLine(ex.TargetSite);

                Console.WriteLine();
                Console.WriteLine(ex.StackTrace);

                Console.WriteLine();

                //if (ex.Data != null)
                //{
                //    foreach (DictionaryEntry item in ex.Data)
                //    {
                //        Console.WriteLine($"{item.Key}, {item.Value}");
                //    }
                //}

                Console.WriteLine(ex.TimeStamp);
                Console.WriteLine(ex.Cause);
            }
            catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Jesteśmy w metodzie finally");
            }

            Console.ReadLine();
        }
    }
}
