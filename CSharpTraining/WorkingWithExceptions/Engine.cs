﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithExceptions
{
    public class Engine
    {
        private bool on;

        public void Power(bool switchOn)
        {
            if (switchOn)
            {
                on = true;
            }
            else
            {
                on = false;
            }
        }

        public bool IsEngineOn()
        {
            return on;
        }
    }
}
