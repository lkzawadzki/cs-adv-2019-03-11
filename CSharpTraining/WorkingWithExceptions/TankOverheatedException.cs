﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithExceptions
{
    [Serializable]
    public class TankOverheatedException : ApplicationException
    {
        public DateTime TimeStamp { get; set; }
        public string Cause { get; set; }

        public TankOverheatedException()
        {

        }

        public TankOverheatedException(string message, string cause, DateTime timeStamp) : base(message)
        {
            Cause = cause;
            TimeStamp = timeStamp;
        }

        public TankOverheatedException(string message, Exception inner) : base(message, inner) { }
    }
}
