﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqBasics
{
    class Program
    {
        static string[] names = { "Kamil Stoch", "Piotr Żyła", "Dawid Kubacki", "Maciek Kot" };
        static int[] numbers = { 2, 15, 67, 4, 6, 15, 4 };

        static void Main(string[] args)
        {
            //BasicLinq();
            //DeferredExecution();
            //CapturedVariables();
            //Subqueries();
            //ProgressiveComposition("o", true, true);
            //ProgressiveComposition();
            Projection();

            Console.ReadLine();
        }

        static void Projection()
        {
            IEnumerable<NameProjectionClass> nameProjections = names.Select(n => new NameProjectionClass
            {
                Original = n,
                Shortened = Shorten(n)
            });

            foreach (NameProjectionClass item in nameProjections)
            {
                Console.WriteLine($"{item.Shortened} powstało z {item.Original}");
            }

            Console.WriteLine();

            var anonymousProjections = names.Select(n => new
            {
                anonOriginal = n,
                anonShortened = Shorten(n)
            });

            foreach (var item in anonymousProjections)
            {
                Console.WriteLine($"{item.anonShortened} powstało z {item.anonOriginal}");
            }

            Console.WriteLine();

            var query1 = from n in names
                         select new
                         {
                             Original = n,
                             Shortened = Shorten(n)
                         }
                         into n1
                         where n1.Shortened.Length >= 8
                         select n1.Original;

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query2 = from n in names
                         let n1 = n.Split().First()
                         let n2 = Shorten(n)
                         where n2.Length >= 8
                         select n1;

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }
        }

        static string Shorten(string fullName)
        {
            return $"{fullName.Split().First().Substring(0, 1)}. {fullName.Split().Last()}";
        }

        private static void ProgressiveComposition()
        {
            var query = names.Select(n => Shorten(n)).Where(n => n.Length >= 8);

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            //var query1 = from n in names
            //             select Shorten(n);

            //var query2 = from n in query1
            //             where n.Length >= 8
            //             select n;

            //var query2 = from n in (from n2 in names select Shorten(n2))
            //             where n.Length >= 8
            //             select n;

            var query2 = from n in names
                         select Shorten(n)
                         into n2
                         where n2.Length >= 8
                         select n2;

            Console.WriteLine();

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }
        }

        static void ProgressiveComposition(string contains, bool isOrder, bool toUpper)
        {
            IEnumerable<string> query = names;

            if (!string.IsNullOrEmpty(contains))
            {
                query = query.Where(n => n.Contains(contains));
            }

            if (isOrder)
            {
                query = query.OrderBy(n => n);
            }

            if (toUpper)
            {
                query = query.Select(n => n.ToUpper());
            }

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

        }

        static void Subqueries()
        {
            var query = names.OrderBy(n => n.Split().Last());

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query1 = names.Where(n => n.Length == names.OrderBy(n2 => n2.Length).First().Length);

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            var query2 = from n in names
                         where n.Length == (from n2 in names orderby n2.Length select n2.Length).First()
                         select n;

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        static void CapturedVariables()
        {
            //int a = 10;

            //var query = numbers.Select(n => n * a);

            //a = 5;

            //foreach (var item in query)
            //{
            //    Console.WriteLine(item);
            //}

            Action[] action = new Action[3];

            for (int i = 0; i < action.Length; i++)
            {
                int j = i;
                action[i] = () => Console.WriteLine(j);
            }

            //int i = 0;
            //action[i] = () => Console.WriteLine(i);
            //i++;
            //action[i] = () => Console.WriteLine(i);
            //i++;
            //action[i] = () => Console.WriteLine(i);
            //i++;


            foreach (var item in action)
            {
                item();
            }

            Console.WriteLine();

            IEnumerable<int> numbers1 = new List<int> { 2, 15, 67, 4, 6, 15, 4 };

            int[] toExclude = { 4, 15 };

            //for (int i = 0; i < toExclude.Length; i++)
            //{
            //    int j = i;

            //    numbers1 = numbers1.Where(n => n != toExclude[j]);
            //}

            foreach (var item in toExclude)
            {
                numbers1 = numbers1.Where(n => n != item);
            }

            foreach (var item in numbers1)
            {
                Console.WriteLine(item);
            }
        }

        static void DeferredExecution()
        {
            var numbers1 = numbers.ToList();

            var query = numbers1.Select(n => n * 10);

            numbers1.Add(15);

            foreach (var item in query)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            numbers1.Add(20);

            foreach (var item in query)
            {
                Console.Write(item + " ");
            }
        }

        static void BasicLinq()
        {
            //IEnumerable<string> query = names.Where(n => n.Length > 10).Where(n => n.Contains("u")).OrderBy(n => n);

            var filteredNames = names.Where(n => n.Length > 10);
            var contains = filteredNames.Where(n => n.Contains("K"));
            var query = contains.OrderBy(n => n[0]);

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            Console.WriteLine(query.Count());
            Console.WriteLine();

            IEnumerable<string> query1 = from n in names
                                         where n.Length > 10
                                         where n.Contains("u")
                                         orderby n
                                         select n;

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            var query2 = numbers.Reverse();

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            int numbersCount = numbers.Count(n => n > 10);

            Console.WriteLine(numbersCount);
        }
    }
}
