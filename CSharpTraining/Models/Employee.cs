﻿using System;
using Newtonsoft.Json;

namespace Models
{
    public enum PerformanceGrade
    {
        None,
        Awful,
        Bad,
        Decent,
        Good,
        Excellent
    }

    [Serializable]
    public class Employee : object, ICloneable
    {
        [JsonProperty]
        private int id;
        public string Name;
        public PerformanceGrade Grade;
        public Contract Contract;

        public Employee()
        {
            Grade = PerformanceGrade.None;
            Contract = new Contract();
        }

        public Employee(int id, string name) : this()
        {
            this.id = id;
            this.Name = name;
        }

        public Employee(int id, string name, PerformanceGrade grade) : this(id, name)
        {
            this.Grade = grade;
        }

        public Employee(int id, string name, PerformanceGrade grade, decimal salary) : this(id, name, grade)
        {
            this.Contract.Salary = salary;
        }

        public Employee(int id, string name, PerformanceGrade grade, decimal salary, DateTime signed, DateTime expires) 
            : this(id, name, grade, salary)
        {
            this.Contract.Signed = signed;
            this.Contract.Expires = expires;
        }

        public int GetId()
        {
            return this.id;
        }

        public void GiveRaise(double percentage)
        {
            this.Contract.Salary = (decimal)((double)this.Contract.Salary * (1 + percentage / 100));
        }

        public object Clone()
        {
            //return new Employee(this.id, this.Name, this.Grade);
            //return this.MemberwiseClone();

            Employee e = (Employee)this.MemberwiseClone();

            Contract c = (Contract)this.Contract.Clone();

            e.Contract = c;

            return e;
        }

        public override string ToString()
        {
            return $"Pracownik: {this.Name}, id: {this.id}, " +
                $"zarabia: {this.Contract.Salary}, ocena: {this.Grade}";
        }

        public override bool Equals(object obj)
        {
            if (obj is Employee && obj != null)
            {
                Employee temp = (Employee)obj;

                if (temp.id == this.id && temp.Name == this.Name && temp.Grade == this.Grade 
                    && this.Contract.Salary == temp.Contract.Salary 
                    && this.Contract.Signed == temp.Contract.Signed 
                    && this.Contract.Expires == temp.Contract.Expires)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
