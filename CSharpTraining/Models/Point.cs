﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public struct Point
    {
        public int x;
        public int y;
        public PointInfo info;

        public Point(int x, int y)
        {
            info = new PointInfo("Punkt nr 1");
            this.x = x;
            this.y = y;
        }

        public void PrintPosition()
        {
            Console.WriteLine($"x: {x}, y: {y}");
        }
    }
}
