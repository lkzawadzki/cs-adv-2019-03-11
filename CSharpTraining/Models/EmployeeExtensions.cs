﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public static class EmployeeExtensions
    {
        public static double GetAverageGrade(this List<Employee> employees)
        {
            int sum = 0;
            int count = 0;

            foreach (var item in employees)
            {
                count++;
                sum += (int)item.Grade;
            }

            if (count > 0)
            {
                return sum / (double)count;
            }

            return 0.0;
        }
    }
}
