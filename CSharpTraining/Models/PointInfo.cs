﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class PointInfo
    {
        public string Description;

        public PointInfo(string description)
        {
            Description = description;
        }
    }
}
