﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Serializable]
    public class Contract : ICloneable
    {
        [JsonProperty]
        private Guid contractId;
        public decimal Salary;
        public DateTime Signed;
        public DateTime Expires;

        public Contract()
        {
            contractId = Guid.NewGuid();

            Salary = 2500M;

            Signed = DateTime.Now;
            Expires = DateTime.Now.AddYears(2);
        }

        public Contract(decimal salary) : this()
        {
            Salary = salary;
        }

        public Contract(decimal salary, DateTime signed, DateTime expires) : this(salary)
        {
            Signed = signed;
            Expires = expires;
        }

        public Guid GetContractId()
        {
            return contractId;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
