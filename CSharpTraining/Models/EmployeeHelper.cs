﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public delegate bool Promotable(Employee employee);

    public static class EmployeeHelper
    {
        public static void CheckForPromotion(List<Employee> list)
        {
            foreach (var item in list)
            {
                if (item.Grade > PerformanceGrade.Decent)
                {
                    Console.WriteLine($"Pracownik {item.Name} zasługuje na podwyżkę!");
                }
            }
        }

        public static void BetterCheckForPromotion(List<Employee> list, Promotable promotable)
        {
            foreach (Employee item in list)
            {
                if (promotable(item))
                {
                    Console.WriteLine($"Pracownik {item.Name} zasługuje na podwyżkę!");
                }
            }
        }

        public static void BestCheckForPromotion(List<Employee> list, Predicate<Employee> pred)
        {
            foreach (Employee item in list)
            {
                if (pred(item))
                {
                    Console.WriteLine($"Pracownik {item.Name} zasługuje na podwyżkę!");
                }
            }
        }
    }
}
