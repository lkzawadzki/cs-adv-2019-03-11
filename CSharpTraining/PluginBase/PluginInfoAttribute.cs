﻿using System;
using System.ComponentModel;

namespace PluginBase
{
    public class PluginInfoAttribute : Attribute
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
