﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplePluginArchitecture
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.openFileDialog1.Filter = ".dll files|*.dll";
            this.textBox1.Text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        }

        private int y = 20;

        private PluginInfoAttribute GetPluginInfo(Type t)
        {
            object info = t.GetCustomAttributes(false).Where(x => x.GetType() == typeof(PluginInfoAttribute)).First();

            return (PluginInfoAttribute)info;
        }

        private void click(object sender, EventArgs e, ITextTransform plugin)
        {
            textBox1.SelectedText = plugin.Transform(textBox1.SelectedText);
        }

        private bool LoadPlugin(string path)
        {
            bool success = false;
            Assembly asm = null;

            try
            {
                asm = Assembly.LoadFrom(path);

                IEnumerable<Type> plugins = asm.GetTypes().Where(t => t.IsClass && t.GetInterface("ITextTransform") != null);

                foreach (Type type in plugins)
                {
                    ITextTransform plugin = (ITextTransform)Activator.CreateInstance(type);

                    Button button = new Button
                    {
                        Text = GetPluginInfo(type).Name,
                        Location = new Point(5, y),
                        Width = 120
                    };

                    y += 25;

                    button.Click += new EventHandler((s, e) => click(s, e, plugin));

                    ToolTip toolTip = new ToolTip();

                    toolTip.SetToolTip(button, GetPluginInfo(type).Description);

                    groupBox1.Controls.Add(button);

                    success = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return success;
        }

        private void loadPluginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (!LoadPlugin(openFileDialog1.FileName))
                {
                    MessageBox.Show("Plugin jest niekompatybilny!");
                }
            }
        }
    }
}
