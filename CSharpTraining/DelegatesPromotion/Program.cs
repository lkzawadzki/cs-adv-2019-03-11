﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesPromotion
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee(1, "Bartosz", PerformanceGrade.Decent, 5000M),
                new Employee(2, "Janusz", PerformanceGrade.Excellent, 7000M),
                new Employee(3, "Grażyna", PerformanceGrade.Good, 6000M)
            };

            EmployeeHelper.CheckForPromotion(employees);
            Console.WriteLine();

            Promotable pr = new Promotable(PromoteByPerformance);

            EmployeeHelper.BetterCheckForPromotion(employees, pr);
            Console.WriteLine();

            EmployeeHelper.BetterCheckForPromotion(employees,
                delegate (Employee emp) { return emp.Grade > PerformanceGrade.Bad; });
            Console.WriteLine();

            EmployeeHelper.BestCheckForPromotion(employees, 
                (Employee emp) => emp.Grade > PerformanceGrade.Decent && emp.Contract.Salary < 7000M);
            Console.WriteLine();

            Func<int, int, string> calculate = (n1, n2) => $"Suma to: {n1 + n2}";

            Console.WriteLine(calculate(10,20));
            Console.WriteLine();

            Console.WriteLine(employees.GetAverageGrade());

            Console.WriteLine();

            employees.ForEach(emp => emp.Grade = PerformanceGrade.None);

            foreach (var item in employees)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadLine();
        }

        static bool PromoteByPerformance(Employee employee)
        {
            return employee.Grade > PerformanceGrade.Good;
        }
    }
}
