﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynamicObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            //ExpandoObjectDemo();
            DynamicObjectDemo();

            Console.ReadLine();
        }

        static void ExpandoObjectDemo()
        {
            dynamic expando = new ExpandoObject();

            expando.Name = "Test obiektu expando";
            expando.Id = 10;
            expando.Print = new Action(() => Console.WriteLine(expando.Id + " " + expando.Name));

            foreach (var item in expando)
            {
                Console.WriteLine(item);
            }

            expando.Print();
        }

        static void DynamicObjectDemo()
        {
            //dynamic t = new Tank();

            //t.Aim();
            //t.Shoot();

            XElement e = XElement.Parse(@"<programmer id='10'><name>Marcin Kozłowski</name></programmer>");

            dynamic attributes = e.DynamicAttributes();

            Console.WriteLine(attributes.id);

            attributes.state = "active";

            Console.WriteLine(e);
        }
    }

    static class XExtension
    {
        public static dynamic DynamicAttributes(this XElement xe)
        {
            return new XWrapper(xe);
        }

        class XWrapper : DynamicObject
        {
            XElement element;

            public XWrapper(XElement e)
            {
                element = e;
            }

            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                result = element.Attribute(binder.Name).Value;
                return true;
            }

            public override bool TrySetMember(SetMemberBinder binder, object value)
            {
                element.SetAttributeValue(binder.Name, value);
                return true;
            }
        }
    }

    class Tank : DynamicObject
    {
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            Console.WriteLine("Została wywołana funkcja: " + binder.Name);
            result = null;
            return true;
        }
    }
}
