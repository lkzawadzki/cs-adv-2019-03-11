﻿using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonInterop
{
    class Program
    {
        static void Main(string[] args)
        {
            //Calculate();
            //Variables();
            Files();

            Console.ReadLine();
        }

        static void Files()
        {
            ScriptEngine engine = Python.CreateEngine();
            ScriptScope scope = engine.CreateScope();

            Employee emp1 = new Employee(3, "Marcin");

            Console.WriteLine(emp1.ToString());

            scope.SetVariable("employee", emp1);

            engine.ExecuteFile(
                @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-11\CSharpTraining\IronPythonApp\IronPythonApp.py",
                scope);

            Console.WriteLine(emp1.ToString());
            Console.WriteLine();

            dynamic type = scope.GetVariable("Person");
            dynamic person = engine.Operations.CreateInstance(type, 6, "Magda");

            person.show();

            dynamic id = person.getId();

            Console.WriteLine(id);

        }

        static void Variables()
        {
            ScriptEngine engine = Python.CreateEngine();
            ScriptScope scope = engine.CreateScope();

            string calculateRaise = "employeeSalary*(1+raisePercentage/100)-employeeSalary";

            scope.SetVariable("employeeSalary", 5000);
            scope.SetVariable("raisePercentage", 5.0);

            ScriptSource source1 = engine
                .CreateScriptSourceFromString(calculateRaise, SourceCodeKind.Expression);

            dynamic result1 = source1.Execute(scope);

            Console.WriteLine(result1);

            string calculateRaise2 = "raise1 = employeeSalary*(1+raisePercentage/100)-employeeSalary";

            ScriptSource source2 = engine.
                CreateScriptSourceFromString(calculateRaise2, SourceCodeKind.SingleStatement);

            source2.Execute(scope);

            dynamic result2 = scope.GetVariable("raise1");

            Console.WriteLine(result2);
        }

        static void Calculate()
        {
            string expression1 = "10*10";
            string expression2 = "[1, 2, 3] + [4, 5]";

            ScriptEngine engine = Python.CreateEngine();

            dynamic result = engine.Execute(expression1);

            Console.WriteLine(result);

            dynamic result2 = engine.Execute(expression2);

            foreach (var item in result2)
            {
                Console.Write(item + " ");
            }
        }
    }
}
