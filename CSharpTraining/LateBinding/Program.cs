﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LateBinding
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly asm = null;

            try
            {
                asm = Assembly.LoadFrom(
                    @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-11\CSharpTraining\Models\bin\Debug\Models.dll");

                //DisplayAssembly(asm);
                BindAssembly(asm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        static void BindAssembly(Assembly asm)
        {
            try
            {
                Type emp1 = asm.GetType("Models.Employee");
                Type grade1 = asm.GetType("Models.PerformanceGrade");

                foreach (var item in emp1.GetConstructors())
                {
                    Console.WriteLine(item);
                }

                Console.WriteLine();

                object o2 = Activator.CreateInstance(grade1);
                
                dynamic o1 = Activator.CreateInstance(emp1, 1, "Janusz", Enum.Parse(o2.GetType(), "4"), 5000M);

                Console.WriteLine("Obiekt o1 został pomyślnie utworzony!");

                Console.WriteLine(o1.ToString());

                object id = o1.GetId();

                Console.WriteLine(id);

                o1.GiveRaise(5.0);

                Console.WriteLine(o1.ToString());

                Type employeeHelper = asm.GetType("Models.EmployeeHelper");

                MethodInfo method3 = employeeHelper.GetMethod("BestCheckForPromotion");

                Type genericList = typeof(List<>);
                Type employeeList = genericList.MakeGenericType(emp1);

                IList o3 = (IList)Activator.CreateInstance(employeeList);
                o3.Add(o1);

                object[] parameters3 = { o3, new Predicate<object>(x => (int)o1.Grade > 3) };

                method3.Invoke(null, parameters3);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //static void BindAssembly(Assembly asm)
        //{
        //    try
        //    {
        //        Type emp1 = asm.GetType("Models.Employee");
        //        Type grade1 = asm.GetType("Models.PerformanceGrade");

        //        foreach (var item in emp1.GetConstructors())
        //        {
        //            Console.WriteLine(item);
        //        }

        //        Console.WriteLine();

        //        object o2 = Activator.CreateInstance(grade1);

        //        object[] paramaters = { 1, "Janusz", Enum.Parse(o2.GetType(), "4"), 5000M };

        //        object o1 = Activator.CreateInstance(emp1, paramaters);

        //        Console.WriteLine("Obiekt o1 został pomyślnie utworzony!");

        //        Console.WriteLine(o1.ToString());

        //        MethodInfo method1 = emp1.GetMethod("GetId");

        //        object id = method1.Invoke(o1, null);

        //        Console.WriteLine(id);

        //        MethodInfo method2 = emp1.GetMethod("GiveRaise");

        //        object[] parameters2 = { 5.0 };

        //        method2.Invoke(o1, parameters2);

        //        Console.WriteLine(o1.ToString());

        //        Type employeeHelper = asm.GetType("Models.EmployeeHelper");

        //        MethodInfo method3 = employeeHelper.GetMethod("BestCheckForPromotion");

        //        Type genericList = typeof(List<>);
        //        Type employeeList = genericList.MakeGenericType(emp1);

        //        IList o3 = (IList)Activator.CreateInstance(employeeList);
        //        o3.Add(o1);

        //        FieldInfo field1 = emp1.GetField("Grade");

        //        object[] parameters3 = { o3, new Predicate<object>(x => (int)field1.GetValue(x) > 3)};

        //        method3.Invoke(null, parameters3);

        //        Console.WriteLine();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //    }
        //}

        private static void DisplayAssembly(Assembly asm)
        {
            if (asm != null)
            {
                Console.WriteLine("-- Lista typów --");

                Console.WriteLine($"{asm.FullName}");

                Type[] types = asm.GetTypes();

                foreach (var item in types)
                {
                    Console.WriteLine($"  {item.Name}");
                }

                Console.WriteLine();
            }
        }
    }
}
