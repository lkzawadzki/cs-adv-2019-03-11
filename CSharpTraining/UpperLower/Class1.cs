﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpperLower
{
    [PluginInfo(Name = "Duże litery", Description = "Zamienia zaznaczony tekst na duże litery")]
    public class ToUpperPlugin : MarshalByRefObject, ITextTransform
    {
        public string Transform(string text)
        {
            //return text.ToUpper();

            string text1 = File.ReadAllText(
                @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-11\CSharpTraining\Serialization\bin\Debug\employee.xml");

            return text1;
        }
    }

    [PluginInfo(Name = "Małe litery", Description = "Zamienia zaznaczony tekst na małe litery")]
    public class ToLowerPlugin : MarshalByRefObject, ITextTransform
    {
        public string Transform(string text)
        {
            return text.ToLower();
        }
    }
}
