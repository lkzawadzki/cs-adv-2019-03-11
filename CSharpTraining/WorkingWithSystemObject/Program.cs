﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithSystemObject
{
    class Program
    {
        static void Main(string[] args)
        {
            DefaultBehaviour();

            Console.ReadLine();
        }

        static void DefaultBehaviour()
        {
            Employee e1 = new Employee(2, "Adam", PerformanceGrade.Excellent, 6000M);

            Console.WriteLine(e1.ToString());
            Console.WriteLine(e1.GetHashCode());
            Console.WriteLine(e1.GetType());

            //Employee e2 = e1;
            Employee e2 = (Employee)e1.Clone();

            Console.WriteLine();
            Console.WriteLine(e1 == e2);
            Console.WriteLine(e1.Equals(e2));

            Console.WriteLine();

            string o1 = "SzkolenieC#";
            string o2 = new string("SzkolenieC#".ToArray());

            Console.WriteLine(o1 == o2);
            Console.WriteLine(o1.Equals(o2));
        }
    }
}
