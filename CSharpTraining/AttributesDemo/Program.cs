﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AttributesDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //UsingEarlyBinding();
            UsingLateBinding();

            Console.ReadLine();
        }

        static void UsingEarlyBinding()
        {
            Type t = typeof(Employee);

            object[] attributes = t.GetCustomAttributes(false);

            foreach (var item in attributes)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        static void UsingLateBinding()
        {
            try
            {
                Assembly asm = Assembly.LoadFrom(
                    @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-11\CSharpTraining\Models\bin\Debug\Models.dll");

                Type[] types = asm.GetTypes();

                foreach (Type type in types)
                {
                    Console.WriteLine(type.Name);

                    object[] attr = type.GetCustomAttributes(false);

                    foreach (var a in attr)
                    {
                        Console.WriteLine($"  {a}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
