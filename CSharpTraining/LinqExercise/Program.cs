﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace LinqExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo[] files;

            using (FileStream fs = new FileStream(
                @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-11\CSharpTraining\LinqExercise\files.dat",
                FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();

                files = (FileInfo[])bf.Deserialize(fs);
            }

            //czy są jakiekolwiek pliki z 2011 roku - true

            Console.WriteLine(files.Any(fi => fi.CreationTime.Year == 2011));

            //znaleźć ilosć plików z roszerzeniem .dll - 2047 lub 2277

            int number = files.Where(fi => string.Compare(fi.Extension, ".dll", true) == 0).Count();

            Console.WriteLine(number);

            //najnowszy .exe - Register-CimProvider.exe

            var date = files.Where(fi => string.Compare(fi.Extension, ".exe", false) == 0).Max(fi => fi.CreationTime);

            var file = files.First(fi => fi.CreationTime == date);

            Console.WriteLine(file.Name + " " + file.CreationTime);

            //pogrupować wg rozszerzenia i wypisać "rozszerzenie - ilość plików"

            var fileStats = from f in files
                            group f by f.Extension.ToLower()
                            into grouping
                            orderby grouping.Key
                            select new { Extension = grouping.Key, Count = grouping.Count() };

            foreach (var item in fileStats)
            {
                Console.WriteLine(item.Extension + "\t - " + item.Count);
            }

            //stworzyć słownik, posortowany wg najczęstszych pierwszych liter pliku i który zawiera 5 elementów klucz-wartość

            var dictionary = (from f in files
                              group f by f.Name.ToLower()[0]
                             into grouping
                              orderby grouping.Count() descending
                              select grouping).Take(5).ToDictionary(g => g.Key, g => g.Count());

            foreach (var item in dictionary)
            {
                Console.WriteLine(item.Key + " - " + item.Value);
            }

            //var query = files.OrderBy(fi => fi.CreationTime);

            //foreach (var item in query)
            //{
            //    Console.WriteLine(item.Name + " " + item.CreationTime);
            //}

            Console.ReadLine();
        }
    }
}
