﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicDemo
{
    class DynamicClass
    {
        private dynamic dynamicField;

        public dynamic DynamicProperty { get; set; }

        public dynamic DynamicMethod(dynamic parameter)
        {
            if (parameter is string)
            {
                return dynamicField;
            }
            else
            {
                return "";
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ImplicitVariables();

            var c = Add(5, 5.5);

            Console.WriteLine(c);

            var d = Add("Wiadomość ", "dla użytkownika");

            Console.WriteLine(d);

            Console.ReadLine();
        }

        static dynamic Add(dynamic a, dynamic b)
        {
            return a + b;
        }

        static void ImplicitVariables()
        {
            //var a = 90;

            //a = "Wiadomość";

            object b = 90;

            Console.WriteLine($"{b.GetType()}, {b}");

            b = "Wiadomość";

            Console.WriteLine($"{b.GetType()}, {b}");

            dynamic c = 90;

            Console.WriteLine($"{c.GetType()}, {c}");

            c = "Wiadomość";

            Console.WriteLine($"{c.GetType()}, {c}");

            Console.WriteLine(c.ToString());

            dynamic o1 = new Employee(1, "Magda");

            Console.WriteLine(o1.GetId());

            Console.WriteLine(o1.GetType());
        }
    }
}
