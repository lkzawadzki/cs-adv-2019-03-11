﻿using LinqToEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqOperators
{
    class Program
    {
        static string[] names = { "Kamil Stoch", "Piotr Żyła", "Dawid Kubacki", "Maciek Kot" };
        static int[] numbers = { 2, 15, 67, 4, 6, 15, 4 };

        static void Main(string[] args)
        {
            //Filtering();
            //Projecting();
            //ProjectingSelectMany();
            //Joining();
            //Grouping();
            //SetOperators();
            //ElementOperators();
            //Aggregation();
            //Quantificators();
            Generating();

            Console.ReadLine();
        }

        static void Generating()
        {
            foreach (var item in Enumerable.Range(10, 10))
            {
                Console.Write(item + " ");
            }

            foreach (var item in Enumerable.Repeat(true, 5))
            {
                Console.Write(item + " ");
            }
        }

        static void Quantificators()
        {
            Console.WriteLine(numbers.Contains(15));

            Console.WriteLine(numbers.Any(n => n > 50));

            Console.WriteLine(numbers.All(n => n > 1));

            int[] numbers1 = { 2, 15, 67, 6, 4, 15, 4 };

            Console.WriteLine(numbers.SequenceEqual(numbers1));
        }

        static void Aggregation()
        {
            Console.WriteLine(numbers.LongCount(n => n > 10));

            Console.WriteLine(numbers.Min(n => n * 10));

            Console.WriteLine(numbers.Sum());

            Console.WriteLine(numbers.Average());
        }

        static void ElementOperators()
        {
            Console.WriteLine(numbers.First());
            Console.WriteLine(numbers.First(n => n > 10 && n < 20));
            Console.WriteLine(numbers.FirstOrDefault(n => n > 100));

            Console.WriteLine(numbers.Single(n => n == 67));
            Console.WriteLine(numbers.SingleOrDefault(n => n == 45));

            Console.WriteLine(numbers.ElementAt(2));
            Console.WriteLine(numbers.ElementAtOrDefault(11));
        }

        static void SetOperators()
        {
            int[] numbers1 = { 15, 4, 10, 28 };

            var concat = numbers.Concat(numbers1);
            var union = numbers.Union(numbers1);

            foreach (var item in concat)
            {
                Console.Write(item + "\t");
            }

            Console.WriteLine();

            foreach (var item in union)
            {
                Console.Write(item + "\t");
            }

            var intersect = numbers.Intersect(numbers1);
            var except = numbers.Except(numbers1);

            Console.WriteLine();

            foreach (var item in intersect)
            {
                Console.Write(item + "\t");
            }

            Console.WriteLine();

            foreach (var item in except)
            {
                Console.Write(item + "\t");
            }
        }

        static void Grouping()
        {
            using (StoreDbContext context = new StoreDbContext())
            {
                var query = from p in context.Products
                            group p by p.Description into g
                            select new { g.Key, Stores = g.Select(p => p.Store.Name) };

                var fluent = context.Products.GroupBy(p => new { p.Description, p.Price })
                    .Select(g => new { g.Key, Stores = g.Select(p => p.Store.Name) });

                foreach (var item in fluent)
                {
                    Console.WriteLine($" - {item.Key}");

                    foreach (var store in item.Stores)
                    {
                        Console.WriteLine($"   - {store}");
                    }
                }
            }
        }

        static void Joining()
        {
            using (StoreDbContext context = new StoreDbContext())
            {
                var query = from s in context.Stores
                            join p in context.Products on s.ID equals p.StoreID
                            where p.Price > 1500M
                            select "Telefon " + p.Description + " można kupić w sklepie: " + s.Name;

                foreach (var item in query)
                {
                    Console.WriteLine(item);
                }

                var query1 = context.Stores.Join(
                    context.Products.Where(p => p.Price > 1500M),
                    s => s.ID,
                    p => p.StoreID,
                    (s, p) => "Telefon " + p.Description + " można kupić w sklepie: " + s.Name + "  - fluent"
                    );

                Console.WriteLine();

                foreach (var item in query1)
                {
                    Console.WriteLine(item);
                }

                Console.WriteLine();

                var query2 = 
                        from s in context.Stores
                        join p in context.Products on s.ID equals p.StoreID
                        into products
                        select new { s.Name, Products = products };

                foreach (var item in query2)
                {
                    Console.WriteLine(item.Name);

                    foreach (var product in item.Products)
                    {
                        Console.WriteLine("  " + product.Description);
                    }
                }
            }

            //metoda Zip

            Console.WriteLine();

            var zip = numbers.Zip(names, (n1, n2) => n1 + ". " + n2);

            foreach (var item in zip)
            {
                Console.WriteLine(item);
            }
        }

        static void ProjectingSelectMany()
        {
            IEnumerable<string[]> query = names.Select(n => n.Split());

            foreach (string[] item in query)
            {
                foreach (string s in item)
                {
                    Console.WriteLine(s);
                }
            }

            Console.WriteLine();

            IEnumerable<string> query1 = names.SelectMany(n => n.Split());

            foreach (string item in query1)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query2 = from n in names
                         from n1 in n.Split()
                         orderby n, n1
                         select n1 + " zostało wybrane z: " + n;

            foreach (string item in query2)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query3 = names.SelectMany(n => n.Split().Select(n1 => new { n1, n }))
                .OrderBy(x => x.n)
                .ThenBy(x => x.n1)
                .Select(x => x.n1 + " zostało wybrane z: " + x.n);

            foreach (string item in query3)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
        }

        static void Projecting()
        {
            var query = names.Select((n, i) => $"{i}: {n}");

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            using (StoreDbContext c = new StoreDbContext())
            {
                var query1 = from s in c.Stores
                             where s.Products.Any(p => p.Price > 1500M)
                            select new
                            {
                                s.Name,
                                Items = from p in s.Products
                                        where p.Price > 1500M
                                        select new
                                        {
                                            ProductName = p.Description,
                                            ProductPrice = p.Price
                                        }
                            };

                var query2 = from s in c.Stores
                             let expensiveProducts = from p in s.Products
                                                     where p.Price > 1500M
                                                     select new
                                                     {
                                                         ProductName = p.Description,
                                                         ProductPrice = p.Price
                                                     }
                             where expensiveProducts.Any()
                             select new { s.Name, Items = expensiveProducts };


                foreach (var item in query2)
                {
                    Console.WriteLine("Sklep: " + item.Name);

                    foreach (var product in item.Items)
                    {
                        Console.WriteLine($"  {product.ProductName}, cena: {product.ProductPrice}");
                    }
                }
            }
        }

        static void Filtering()
        {
            var query = names.Where(n => n.EndsWith("a"));

            Console.WriteLine(query.Any());
            Console.WriteLine();

            var query1 = names.Where((n, i) => i % 2 == 0);

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query2 = names.Take(2);

            var query3 = names.Skip(2);

            foreach (var item in query3)
            {
                Console.WriteLine(item);
            }

            var query4 = numbers.TakeWhile(n => n < 50);

            Console.WriteLine();

            foreach (var item in query4)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            var query5 = numbers.SkipWhile(n => n < 50);

            foreach (var item in query5)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            var query6 = numbers.Distinct();

            foreach (var item in query6)
            {
                Console.Write(item + " ");
            }

        }
    }

    //class Context
    //{
    //    public List<Product> Products { get; set; }
    //    public List<Store> Stores { get; set; }

    //    public Context()
    //    {

    //    }
    //}
}
