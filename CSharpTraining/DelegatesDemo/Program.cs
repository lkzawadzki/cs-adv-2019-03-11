﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesDemo
{
    delegate void MessageDelegate(string text);

    class Program
    {     
        static void Main(string[] args)
        {
            MessageDelegate md = new MessageDelegate(PrintMessage);

            md("Wiadomość z delegatu");

            Console.ReadLine();
        }

        static void PrintMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
